//
//  DetailsViewController.swift
//  Movies
//
//  Created by GS Track Me Dev on 11/03/21.
//

import UIKit
import Kingfisher

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var movieList: UITableView!
    @IBOutlet weak var moviePoster: UIImageView!
    
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var userRating: UILabel!
    @IBOutlet weak var originalTitle: UILabel!
    
    @IBOutlet weak var sinopsis: UILabel!
    
    var movieDetails : Movie?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setData()
    }
}

extension DetailsViewController {
    
    ///
    ///* Original title
    ///* Miniature image of the poster
    ///* Synopsis (general view taken from the API)
    ///* User rating or rating (API)
    ///* Release date
    ///
    
    func setData() {
        if let movieTitle = movieDetails?.original_title, let avg = movieDetails?.vote_average, let overview =
            movieDetails?.overview, let imgPath = movieDetails?.poster_path, let movieRelease = movieDetails?.release_date {
            originalTitle.text = movieTitle
            userRating.text = "\(avg)/10"
            sinopsis.text = overview
            releaseDate.text = movieRelease
            
            let url = URL(string: movieEndpoint.imageUrl(path: imgPath))
            let processor = DownsamplingImageProcessor(size: self.moviePoster.bounds.size)
                         |> RoundCornerImageProcessor(cornerRadius: 0)
            self.moviePoster.contentMode = .scaleAspectFit
            self.moviePoster.kf.indicatorType = .activity
            self.moviePoster.kf.setImage(
                with: url,
                placeholder: UIImage(named: "image_placeholder"),
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.flipFromRight(1)),
                    .cacheOriginalImage
                ])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
    }
}

extension DetailsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "trailerCell")!
            
        return cell
    }
}

extension DetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
