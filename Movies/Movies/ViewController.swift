//
//  ViewController.swift
//  Movies
//
//  Created by GS Track Me Dev on 06/03/21.
//

import UIKit
import Kingfisher

class ViewController: UIViewController {
    
    @IBOutlet weak var movieList: UICollectionView!
    @IBOutlet weak var rightMovieListConstraint: NSLayoutConstraint!
    
    let viewModel = MovieViewModel()
    
    var mFirstStart = true
    
    var selected : IndexPath?
    
    var filterType = filter.popularity.filterName
    
    var filterTitle = filter.popularity.valueName

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        prepareUI()
        fetchData()
    }
    
    func prepareUI() {
        self.title = NSLocalizedString(filterType, comment: "popular").uppercased()
        prepareButton()
        prepareViewModelObserver()
    }
}

//MARK: Delegate and Datasource

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.movies?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : MovieCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMovie", for: indexPath) as! MovieCollectionViewCell
        
        if let movieInfo = viewModel.movies?[indexPath.row], let imgPath = movieInfo.poster_path {
            let url = URL(string: movieEndpoint.imageUrl(path: imgPath))
            let processor = DownsamplingImageProcessor(size: cell.imagePoster?.bounds.size ?? .zero)
                         |> RoundCornerImageProcessor(cornerRadius: 5)
            cell.imagePoster?.contentMode = .scaleAspectFit
            cell.imagePoster?.kf.indicatorType = .activity
            cell.imagePoster?.kf.setImage(
                with: url,
                placeholder: UIImage(named: "image_placeholder"),
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
        
        return cell
    }
}

extension ViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selected = indexPath
        performSegue(withIdentifier: "segueDetails", sender: nil)
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var gridsize : Float = 2.0
        //var padding : Float = 15.0
        if UIDevice.current.orientation.isLandscape {
            gridsize = 3.0
        }
        let width = ((Float(collectionView.frame.width) - 15) / gridsize)
        print("cell width : \(width)")
        return CGSize(width: Double(width), height: Double(width) * 1.5)
    }
}

extension ViewController {
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            if UIApplication.shared.statusBarOrientation.isLandscape {
                // activate landscape changes
                print("orientantion changed to landscape")
                
                //UIView.animate(withDuration: 0.5) {
                    //self.view.layoutIfNeeded()
                //}
                self.rightMovieListConstraint.constant = (self.view.safeAreaFrame.width / 2) * -1;
            } else {
                // activate portrait changes
                print("orientation is now portrait")
                
                //UIView.animate(withDuration: 0.5) {
                    //self.view.layoutIfNeeded()
                //}
                self.rightMovieListConstraint.constant = 0
            }
        })
        self.movieList.reloadData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if (mFirstStart) {
            mFirstStart = false
            detectOrientation()
        }
    }

    func detectOrientation() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
                self.rightMovieListConstraint.constant = (self.view.safeAreaFrame.width / 2) * -1;
                //self.movieList.reloadData()
            }
        } else {
            print("Portrait")
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
                self.rightMovieListConstraint.constant = 0
                //self.movieList.reloadData()
            }
        }
    }
}

//MARK: Loading methods

extension ViewController {
    
    func fetchData() {
        viewModel.loadMoviesFromAPI(useFilter: filterType)
    }
    
    func prepareViewModelObserver() {
        viewModel.moviesDidUpdate = { (finished, error) in
            if !error {
                self.reloadView()
            }
        }
    }
    
    func prepareButton() {
        let icon = UIImage(named: "filterIcon")
        let filterButton = UIBarButtonItem(image: icon, style: .plain, target: self, action: #selector(filterSearch))
        navigationItem.rightBarButtonItems = [filterButton]
    }
    
    func reloadView() {
        movieList.reloadData()
    }
    
    @objc func filterSearch() {
        if filterType == filter.popularity.filterName {
            filterType = filter.rating.filterName
            filterTitle = NSLocalizedString(filter.rating.filterName, comment: "top_rate").uppercased()
        }
        else {
            filterType = filter.popularity.filterName
            filterTitle = NSLocalizedString(filter.popularity.filterName, comment: "popular").uppercased()
        }
        self.title = filterTitle.uppercased()
        viewModel.loadMoviesFromAPI(useFilter: filterType)
    }
}

//MARK: Navigation

extension ViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetails"
        {
            if let indexPath = self.selected {
                let detailsVC = segue.destination as! DetailsViewController
                detailsVC.movieDetails = viewModel.movies?[indexPath.row]
            }
        }
    }
}
