//
//  Utils.swift
//  Movies
//
//  Created by GS Track Me Dev on 11/03/21.
//

import Foundation

extension UIView {
    public var safeAreaFrame: CGRect {
        if #available(iOS 11, *) {
            return safeAreaLayoutGuide.layoutFrame
        }
        return bounds
    }
}
