//
//  MovieViewModel.swift
//  Movies
//
//  Created by GS Track Me Dev on 08/03/21.
//

import Foundation
import UIKit
import Alamofire

protocol MovieVMProtocol {
    
    var moviesDidUpdate : ((Bool, Bool) -> Void)? { get set}
    func loadMoviesFromAPI(useFilter: String)
}

struct filter {
    static let popularity = (filterName: "popular", valueName: "Popular")
    static let rating = (filterName: "top_rated", valueName: "Top rated")
}

class MovieViewModel: MovieVMProtocol {
    
    var moviesDidUpdate: ((Bool, Bool) -> Void)?
    
    var movies: [Movie]? {
        didSet {
            self.moviesDidUpdate?(true, false)
        }
    }
    
    func loadMoviesFromAPI(useFilter: String) {
        
        self.movies = [Movie]()

        let requestWithParameters = AF.request(movieEndpoint.customUrl(filter: useFilter), method: .get)
            
        requestWithParameters.responseDecodable(of: MovieResponseModel.self) { (response) in
            guard let movieResponse = response.value else {
                self.moviesDidUpdate?(true, true)
                return
            }
            print(movieResponse)
            self.movies = movieResponse.movie ?? []
        }
    }
}
