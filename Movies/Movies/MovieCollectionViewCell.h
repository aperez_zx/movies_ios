//
//  MovieCollectionViewCell.h
//  Movies
//
//  Created by GS Track Me Dev on 10/03/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MovieCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imagePoster;

@end

NS_ASSUME_NONNULL_END
