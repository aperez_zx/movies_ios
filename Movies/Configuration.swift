//
//  Configuration.swift
//  Movies
//
//  Created by GS Track Me Dev on 10/03/21.
//

struct urlDomain {
    static let base = "https://api.themoviedb.org/3/movie/"
    static let baseIMG = "https://image.tmdb.org/t/p/"
}

struct movieEndpoint {
    static let defaultSize = "w185/"
    static let customscheme = "?api_key="
}

extension movieEndpoint {
    static func imageUrl(path : String) -> String {
        return urlDomain.baseIMG + defaultSize + path
    }
    static func customUrl(filter: String) -> String {
        return urlDomain.base + filter + customscheme + apiKey.dev
    }
}

struct apiKey {
    static let dev = "6a99a94ff9c69a8726083023de1b4ad7"
    static let prod = "6a99a94ff9c69a8726083023de1b4ad7" //No production key required
}
